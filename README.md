# C99 Benchmarks

My personal benchmarks for c99, just some tests I do to know the impact of specific choices like virtual calls vs jump tables, some because I'm curious, some because I want to see the numbers. Results below for reference, worth emphasizing that these are casual results, Not in an isolated environment, not a clean boot, no warmup runs, the SIZE is not big enough too. 

System:
Linux Zen 6.4.6

CPU:
Ryzen 5 3600 (Overclocked 4.2Ghz On-demand)

Results Jul 31 2023

```
SIZE 100000
gcc -ln -O0 main.c
[==========] Running 4 benchmarks.
[ RUN      ] procedures.iterate_find_callback
[       OK ] procedures.iterate_find_callback (mean 376.024us, confidence interval +- 1.254852%)
[ RUN      ] procedures.iterate_find_allocate_list
[       OK ] procedures.iterate_find_allocate_list (mean 967.119us, confidence interval +- 0.286852%)
[ RUN      ] procedures.virtual_call
[       OK ] procedures.virtual_call (mean 358.821us, confidence interval +- 0.230973%)
[ RUN      ] procedures.jump_table
[       OK ] procedures.jump_table (mean 286.238us, confidence interval +- 0.574906%)
[==========] 4 benchmarks ran.
[  PASSED  ] 4 benchmarks.
```

```
SIZE 100000
gcc -ln -O2 main.c
[==========] Running 4 benchmarks.
[ RUN      ] procedures.iterate_find_callback
[       OK ] procedures.iterate_find_callback (mean 37.425us, confidence interval +- 0.233841%)
[ RUN      ] procedures.iterate_find_allocate_list
[       OK ] procedures.iterate_find_allocate_list (mean 481.451us, confidence interval +- 0.457836%)
[ RUN      ] procedures.virtual_call
[       OK ] procedures.virtual_call (mean 121.283us, confidence interval +- 0.111676%)
[ RUN      ] procedures.jump_table
[       OK ] procedures.jump_table (mean 49.602us, confidence interval +- 0.256333%)
[==========] 4 benchmarks ran.
[  PASSED  ] 4 benchmarks.

```
