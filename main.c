#include "ubench.h"
#include <stdlib.h>

UBENCH_MAIN();

struct obj {
	int id;
	int f1;
	int f2;
};

#define SIZE 100000

typedef void (*obj_cb)(struct obj *obj);
static void obj_callback(struct obj *obj)
{
	obj->f1 = 10;
	obj->f2 = 10;
}

UBENCH_EX(procedures, iterate_find_callback)
{
	struct obj *objs = malloc(SIZE * sizeof(struct obj));
	objs[2000].id = 0;
	objs[4000].id = 0;
	objs[4001].id = 0;
	obj_cb cb = obj_callback;
	UBENCH_DO_BENCHMARK()
	{
		for (int i = 0; i < SIZE; i++) {
			if (objs[i].id == 0) {
				cb(&objs[i]);
			}
		}
	}
	free(objs);
}

UBENCH_EX(procedures, iterate_find_allocate_list)
{
	struct obj *objs = malloc(SIZE * sizeof(struct obj));
	objs[2000].id = 0;
	objs[4000].id = 0;
	objs[4001].id = 0;
	UBENCH_DO_BENCHMARK()
	{
		int cnt = 0;
		for (int i = 0; i < SIZE; i++) {
			if (objs[i].id == 0) {
				cnt++;
			}
		}
		int y = 0;
		struct obj *obj_list = malloc(sizeof(struct obj) * cnt);
		for (int i = 0; i < SIZE; i++) {
			if (objs[i].id == 0) {
				obj_list[y] = obj_list[i];
				y++;
			}
		}

		for (int i = 0; i < cnt; i++) {
			obj_list[i].f1 = 10;
			obj_list[i].f2 = 10;
		}
	}
	free(objs);
}

struct obj2;
typedef void (*obj2_cb)(struct obj2 *obj);
struct obj2 {
	obj2_cb cb;
	int f1;
	int f2;
};
struct obj3 {
	int type;
	int f1;
	int f2;
};

static void obj2_call0(struct obj2 *obj)
{
	obj->f1 = 10;
	obj->f2 = 10;
}

static void obj2_call1(struct obj2 *obj)
{
	obj->f1 = 20;
	obj->f2 = 20;
}

static void obj2_call2(struct obj2 *obj)
{
	obj->f1 = 30;
	obj->f2 = 30;
}

UBENCH_EX(procedures, virtual_call)
{
	struct obj2 *objs = malloc(sizeof(struct obj2) * SIZE);

	for (int i = 0; i < SIZE; i++) {
		objs[i].cb = obj2_call0;
	}

	objs[2000].cb = obj2_call1;
	objs[4000].cb = obj2_call1;
	objs[4001].cb = obj2_call1;
	objs[5000].cb = obj2_call2;

	UBENCH_DO_BENCHMARK()
	{
		for (int i = 0; i < SIZE; i++) {
			objs[i].cb(&objs[i]);
		}
	}
	free(objs);
}

static void call0(struct obj3 *obj)
{
	obj->f1 = 10;
	obj->f2 = 10;
}

static void call1(struct obj3 *obj)
{
	obj->f1 = 20;
	obj->f2 = 20;
}

static void call2(struct obj3 *obj)
{
	obj->f1 = 30;
	obj->f2 = 30;
}

UBENCH_EX(procedures, jump_table)
{
	struct obj3 *objs = malloc(sizeof(struct obj3) * SIZE);

	for (int i = 0; i < SIZE; i++) {
		objs[i].type = 0;
	}

	objs[2000].type = 1;
	objs[4000].type = 1;
	objs[4001].type = 1;
	objs[5000].type = 2;

	UBENCH_DO_BENCHMARK()
	{
		for (int i = 0; i < SIZE; i++) {
			switch (objs[i].type) {
			case 0:
				call0(&objs[i]);
				break;
			case 1:
				call1(&objs[i]);
				break;
			case 2:
				call2(&objs[i]);
				break;
			default:
				break;
			}
		}
	}
	free(objs);
}

static void obj2_call_dummy(struct obj2 *obj)
{
	(void)obj;
}

// 
// Compares an empty callback, acting as a dummy value for instead of NULL
// This allows us to avoid the NULL check and just call an empty callback
// 
UBENCH_EX(procedures, empty_callback)
{
	struct obj2 *objs = malloc(sizeof(struct obj2) * SIZE);

	for (int i = 0; i < SIZE; i++) {
		objs[i].cb = obj2_call0;
	}

	objs[2000].cb = NULL;
	objs[4000].cb = NULL;
	objs[4001].cb = NULL;
	objs[5000].cb = NULL;

	UBENCH_DO_BENCHMARK()
	{
		for (int i = 0; i < SIZE; i++) {
			if (objs[i].cb != NULL)
				objs[i].cb(&objs[i]);
		}
	}
	free(objs);
}
UBENCH_EX(procedures, null_check)
{
	struct obj2 *objs = malloc(sizeof(struct obj2) * SIZE);

	for (int i = 0; i < SIZE; i++) {
		objs[i].cb = obj2_call0;
	}

	objs[2000].cb = obj2_call_dummy;
	objs[4000].cb = obj2_call_dummy;
	objs[4001].cb = obj2_call_dummy;
	objs[5000].cb = obj2_call_dummy;

	UBENCH_DO_BENCHMARK()
	{
		for (int i = 0; i < SIZE; i++) {
			objs[i].cb(&objs[i]);
		}
	}
	free(objs);
}
